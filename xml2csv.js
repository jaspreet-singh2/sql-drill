const { writeFileSync } = require('fs');
let converter = require('json-2-csv');
var convert = require('xml-js');
var xml = require('fs').readFileSync('./Comments.xml', 'utf8');
async function main()
{
   
    
    var result = convert.xml2json(xml, {compact: true, spaces: 4});
    writeFileSync('./Comments.json',result)
    result =JSON.parse(result)
    
    const csv = await converter.json2csv(result.comments.row);
    writeFileSync('./Comments.csv',csv)
}
main()