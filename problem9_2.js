const { Client } = require("pg");
const client = new Client({
  user: "postgres",
  host: "127.0.0.1",
  database: "postgres",
  password: "wood",
  port: 5432,
});
client.connect(function (err) {
  if (err) throw err;
  console.log("Connected!");
});

async function main() {
   
  const result = await client.query(`
  select points.userid,(coalesce(ud2points,0)+coalesce(postscount,0)+coalesce(cpoints,0)) as total_points from (
  ((select count(owneruserid)*10 as postscount, owneruserid as userid2 from posts group by owneruserid ) Pp
   FULL OUTER JOIN
   (select (_attributes_upvotes+_attributes_downvotes) as UD2points ,_attributes_id as userid from user_data) UDp on (Pp.userid2 = CAST( UDp.userid AS int))) points
    FULL OUTER JOIN
    (select count(userid)*3 as Cpoints ,userid   from comments group by userid order by Cpoints desc) cPoints on (CAST( points.userid AS int) = cpoints.userid ))   order by total_points desc limit 5;  
    
   `);
  
 console.log(result.rows)
  await client.end()
}
main()