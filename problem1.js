const { Client } = require("pg");
const client = new Client({
  user: "postgres",
  host: "127.0.0.1",
  database: "postgres",
  password: "wood",
  port: 5432,
});
client.connect(function (err) {
  if (err) throw err;
  console.log("Connected!");
});

async function main() {
    const postGreaterThanOne = await client.query(`select COUNT(*) from posts where posttypeid = 1 AND answercount >=1;`)
    const postLessThanOne = await client.query('select COUNT(*) from posts where posttypeid = 1;')
    console.log((Number(postGreaterThanOne.rows[0].count)/Number(postLessThanOne.rows[0].count))*100)
    await client.end()
}
main()