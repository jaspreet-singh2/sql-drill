const { Client } = require("pg");
const client = new Client({
  user: "postgres",
  host: "127.0.0.1",
  database: "postgres",
  password: "wood",
  port: 5432,
});
client.connect(function (err) {
  if (err) throw err;
  console.log("Connected!");
});

async function main() {
   
  const result = await client.query(`with subq as
  (
      select count (*) as usage  , extract(year from creationdate)as years from posts where posttypeid =1  group by extract(year from creationdate) order by years
  )
  select *  from subq  where usage =(select  max(usage) from subq);`);
  
 console.log(result.rows)
  await client.end()
}
main()